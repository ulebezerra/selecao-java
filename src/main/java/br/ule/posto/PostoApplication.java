package br.ule.posto;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class PostoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PostoApplication.class, args);
	}

}