package br.ule.posto.model;

public enum Unidade {

	LITRO,
	METRO_CUBICO
}