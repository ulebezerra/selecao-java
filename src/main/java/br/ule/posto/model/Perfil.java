package br.ule.posto.model;

public enum Perfil {

	ADMIN,
	FUNCIONARIO,
	CLIENTE
}