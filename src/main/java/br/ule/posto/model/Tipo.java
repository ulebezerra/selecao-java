package br.ule.posto.model;

public enum Tipo {

	DIESEL,
	ETANOL,
	GASOLINA,
	GNV
}