package br.ule.posto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ule.posto.model.Produto;

public interface ProdutoRepository extends JpaRepository<Produto, Long>{

}