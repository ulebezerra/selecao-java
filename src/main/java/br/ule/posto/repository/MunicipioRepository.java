package br.ule.posto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ule.posto.model.Municipio;

public interface MunicipioRepository  extends JpaRepository<Municipio, Long>{

	Municipio findByNome(String municipioNome);

}