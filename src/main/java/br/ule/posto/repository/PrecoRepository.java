package br.ule.posto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ule.posto.model.Preco;

public interface PrecoRepository extends JpaRepository<Preco, Long>{

	List<Preco> findByAtivo(boolean ativo);
}