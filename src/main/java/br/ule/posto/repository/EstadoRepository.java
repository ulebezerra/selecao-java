package br.ule.posto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ule.posto.model.Estado;
import br.ule.posto.model.Regiao;

public interface EstadoRepository extends JpaRepository<Estado, Long>{
	
	List<Estado> findByRegiao(Regiao regiao);
	Estado findBySigla(String sigla);
}