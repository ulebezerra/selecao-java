package br.ule.posto.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ule.posto.model.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long>{

	List<Usuario> findByCpf(String cpf);
}