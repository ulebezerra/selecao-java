package br.ule.posto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ule.posto.model.Distribuidora;

public interface DistribuidoraRepository extends JpaRepository<Distribuidora, Long>{
	Distribuidora findByCodigo(Long codigo);
}