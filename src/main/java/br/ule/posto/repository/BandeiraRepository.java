package br.ule.posto.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.ule.posto.model.Bandeira;

public interface BandeiraRepository extends JpaRepository<Bandeira, Long>{

	Bandeira findByNome(String nome);
}