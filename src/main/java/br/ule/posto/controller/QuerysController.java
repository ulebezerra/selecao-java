package br.ule.posto.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.ule.posto.controller.dto.DistribuidoraDto;
import br.ule.posto.controller.dto.EstadoDto;
import br.ule.posto.model.Distribuidora;
import br.ule.posto.model.Estado;
import br.ule.posto.model.Municipio;
import br.ule.posto.model.Preco;
import br.ule.posto.model.Produto;
import br.ule.posto.model.Regiao;
import br.ule.posto.repository.DistribuidoraRepository;
import br.ule.posto.repository.EstadoRepository;
import br.ule.posto.repository.MunicipioRepository;

@RestController
@RequestMapping("/query")
public class QuerysController {
	
	@Autowired
	private MunicipioRepository municipioRepository;
	
	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private DistribuidoraRepository distribuidoraRepository;

	@GetMapping("/mediaPorMunicipio/{municipioNome}")
	public ResponseEntity<String> mediaPorMunicipio(@PathVariable String municipioNome) {
		float total = 0;
		float quantidade = 0;
		Municipio municipio = municipioRepository.findByNome(municipioNome);
		
		if(municipio != null) {
			for (Distribuidora distribuidora : municipio.getDistribuidoras()) {
				for (Produto produto : distribuidora.getProdutos()) {
					for (Preco preco : produto.getPrecos()) {
						total += preco.getValor();
						quantidade++;
					}
				}
			}
			return new ResponseEntity<>(String.valueOf(total/quantidade) , HttpStatus.OK);
		}
		return ResponseEntity.notFound().build();
	}
	
	@GetMapping("/descRegiao/{regiao}")
	public List<EstadoDto> descRegiao(@PathVariable Regiao regiao) {
		List<Estado> estados = estadoRepository.findByRegiao(regiao);
		return EstadoDto.converter(estados);
	}
	
	@GetMapping("/descDistribuidora/{distribuidoraId}")
	public DistribuidoraDto descDistribuidora(@PathVariable Long distribuidoraId) {
		Distribuidora distribuidora = distribuidoraRepository.getOne(distribuidoraId);
		return new DistribuidoraDto(distribuidora);	
	}
}