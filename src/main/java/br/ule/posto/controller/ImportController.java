package br.ule.posto.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import br.ule.posto.model.Bandeira;
import br.ule.posto.model.Distribuidora;
import br.ule.posto.model.Estado;
import br.ule.posto.model.Municipio;
import br.ule.posto.model.Preco;
import br.ule.posto.model.Produto;
import br.ule.posto.model.Regiao;
import br.ule.posto.model.Tipo;
import br.ule.posto.model.Unidade;
import br.ule.posto.repository.BandeiraRepository;
import br.ule.posto.repository.DistribuidoraRepository;
import br.ule.posto.repository.EstadoRepository;
import br.ule.posto.repository.MunicipioRepository;
import br.ule.posto.repository.PrecoRepository;
import br.ule.posto.repository.ProdutoRepository;
import br.ule.posto.utils.CsvUtils;
import br.ule.posto.utils.DateUtils;

@RestController
@RequestMapping("/import")
public class ImportController {

	@Autowired
	private EstadoRepository estadoRepository;
	
	@Autowired
	private MunicipioRepository municipioRepository;
	
	@Autowired
	private DistribuidoraRepository distribuidoraRepository;
	
	@Autowired
	private BandeiraRepository bandeiraRepository;
	
	@Autowired
	private ProdutoRepository produtoRepository;
	
	@Autowired
	private PrecoRepository precoRepository;
	
    @PostMapping(value = "/upload", consumes = "multipart/form-data")
    public String uploadMultipart(@RequestParam("file") MultipartFile file) throws IllegalStateException, IOException {
    	
    	List<String> result = CsvUtils.read(file);
    	int invalidos = 0;
    	
    	for ( String row : result.subList( 1, result.size() ) )
    	{
    		 List<String> valores = CsvUtils.convertRow(row);
 
    		 if(valores.size() == 11 && CsvUtils.validacaoParse(valores)) {
	    		 Estado estado = estadoRepository.findBySigla(valores.get(1));
	    		 Municipio municipio = municipioRepository.findByNome(valores.get(2));
	    		 Distribuidora distribuidora = distribuidoraRepository.findByCodigo(Long.parseLong(valores.get(4)));
	    		 Bandeira bandeira = bandeiraRepository.findByNome(valores.get(10));
	    		 List<Produto> produtos = produtoRepository.findAll();
	    		 Produto produto = null;
	    		 
	    		 if(estado == null) {
	    			 estado = new Estado();
	    			 estado.setRegiao(Regiao.valueOf(valores.get(0)));
	    			 estado.setSigla(valores.get(1));
	    			 estadoRepository.save(estado);
	    		 }
	    		 
	    		 if(municipio == null) {
	    			 municipio = new Municipio();
	    			 municipio.setNome(valores.get(2));
	    			 municipio.setEstado(estado);
	    			 municipioRepository.save(municipio);
	    		 }
	    		 
	    		 if(distribuidora == null) {
	    			 distribuidora = new Distribuidora();
	    			 distribuidora.setNome(valores.get(3));
	    			 distribuidora.setCodigo(Long.parseLong(valores.get(4)));
	    			 distribuidora.setMunicipio(municipio);
	    			 distribuidoraRepository.save(distribuidora);
	    		 }
	    		 
	    		 if(bandeira == null) {
	    			 bandeira = new Bandeira();
	    			 bandeira.setNome(valores.get(10));
	    			 bandeiraRepository.save(bandeira);
	    		 }
	    		 
	    		 for (Produto produtoInterator : produtos) {
	 				if(produtoInterator.getTipo() == Tipo.valueOf(valores.get(5)) && produtoInterator.getDistribuidora().getId() == distribuidora.getId()) {
	 					produto = produtoInterator;
	 					}
	 			 }
	     		 if(produto == null) {
	     			 produto = new Produto();
	     			 produto.setTipo(Tipo.valueOf(valores.get(5)));
	     			 produto.setUnidade(Unidade.LITRO);
	     			 produto.setDistribuidora(distribuidora);
	     			 produto.setBandeira(bandeira);
	     		 }
	     		 produtoRepository.save(produto);
 
     		 	Preco preco = new Preco(Double.valueOf(valores.get(7).replaceAll(",",".")), Double.valueOf(valores.get(8).replaceAll(",",".")), DateUtils.converteDate((valores.get(6))), produto);
     		 	precoRepository.save(preco);
     		 	
    		 } else {
    			 invalidos++;
    		 }
    	}
    	return "Registros invalidos:"+ invalidos;
    }
}