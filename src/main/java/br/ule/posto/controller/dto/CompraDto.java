package br.ule.posto.controller.dto;

import br.ule.posto.model.Compra;
import br.ule.posto.model.Produto;
import br.ule.posto.model.Usuario;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class CompraDto {
	
	private Long id;
	private Produto produto;
	private Usuario usuario;
	private int quantidade;
	
	public CompraDto(Compra compra) {
		this.id = compra.getId();
		this.produto = compra.getProduto();
		this.usuario = compra.getUsuario();
		this.quantidade = compra.getQuantidade();
	}
}