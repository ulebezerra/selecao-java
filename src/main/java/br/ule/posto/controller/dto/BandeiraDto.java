package br.ule.posto.controller.dto;

import br.ule.posto.model.Bandeira;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class BandeiraDto {

	private Long id;
	private String nome;
	
	public BandeiraDto(Bandeira bandeira) {
		this.id = bandeira.getId();
		this.nome = bandeira.getNome();
	}
}