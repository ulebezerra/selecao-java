package br.ule.posto.controller.dto;

import java.time.LocalDateTime;

import br.ule.posto.model.Preco;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class DetalharPrecoDto {

	private Long id;
	private double valor;
	private double valorVenda;
	private boolean ativo;
	private LocalDateTime dataColeta;	
	private LocalDateTime dataCriacao;
	private String produtoNome;
	
	public DetalharPrecoDto(Preco preco)
	{
		super();
		this.id = preco.getId();
		this.valor = preco.getValor();
		this.valorVenda = preco.getValorVenda();
		this.ativo = preco.isAtivo();
		this.dataColeta = preco.getDataColeta();
		this.dataCriacao = preco.getDataCriacao();
		this.produtoNome = preco.getProduto().getTipo().name();
	}
}
