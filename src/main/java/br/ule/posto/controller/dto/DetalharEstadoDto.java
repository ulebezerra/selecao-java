package br.ule.posto.controller.dto;

import java.util.ArrayList;
import java.util.List;

import br.ule.posto.model.Estado;
import br.ule.posto.model.Municipio;
import br.ule.posto.model.Regiao;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class DetalharEstadoDto {

	
	private Long id;
	private String sigla;
	private String nome;
	private Regiao regiao;
	private List<Municipio> municipios = new ArrayList<>();
	
	public DetalharEstadoDto(Estado estado) {
		this.id = estado.getId();
		this.sigla = estado.getSigla();
		this.nome = estado.getNome();
		this.regiao = estado.getRegiao();
		this.municipios = estado.getMunicipios();
	}
}
