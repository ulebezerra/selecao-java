package br.ule.posto.controller.dto;

import java.util.ArrayList;
import java.util.List;

import br.ule.posto.model.Distribuidora;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class DistribuidoraDto {

	private Long id;
	private String nome;
	private Long codigo;
	private List<ProdutoDto> produtos = new ArrayList<>();
	
	public static List<DistribuidoraDto> converter(List<Distribuidora> distribuidoras) {
		List<DistribuidoraDto> usuariosDto = new ArrayList<>();
		for (Distribuidora distribuidora : distribuidoras) {
			usuariosDto.add(new DistribuidoraDto(distribuidora.getId(), distribuidora.getNome(), distribuidora.getCodigo(), ProdutoDto.converter(distribuidora.getProdutos())));
		}
		return usuariosDto;
	}
	
	public DistribuidoraDto(Distribuidora distribuidora) {
		this.id = distribuidora.getId();
		this.nome = distribuidora.getNome();
		this.codigo = distribuidora.getCodigo();
	}
}