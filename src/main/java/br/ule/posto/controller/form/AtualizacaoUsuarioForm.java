package br.ule.posto.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.ule.posto.model.Perfil;
import br.ule.posto.model.Usuario;
import br.ule.posto.repository.UsuarioRepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class AtualizacaoUsuarioForm {
	
	@NotNull @NotEmpty
	private String cpf;
	@NotNull @NotEmpty
	private String nome;
	private Perfil perfil;
	
	public Usuario atualizar(Long id, UsuarioRepository usuarioRepository) {
		Usuario usuario = usuarioRepository.getOne(id);
		usuario.setCpf(this.cpf);
		usuario.setNome(this.nome);
		usuario.setPerfil(this.perfil);
		
		return usuario;
	}
}