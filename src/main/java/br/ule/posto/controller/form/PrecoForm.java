package br.ule.posto.controller.form;

import java.time.LocalDateTime;

import br.ule.posto.model.Preco;
import br.ule.posto.model.Produto;
import br.ule.posto.repository.ProdutoRepository;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class PrecoForm {
	
	private float valor;
	private float valorVenda;
	private LocalDateTime dataColeta;
	private Long produtoId;
	
	public Preco converter(ProdutoRepository produtoRepository) {

		Produto produto = produtoRepository.getOne(produtoId);
		return new Preco(valor, valorVenda, dataColeta, produto);
	}
}