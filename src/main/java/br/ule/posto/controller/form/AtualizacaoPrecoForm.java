package br.ule.posto.controller.form;

import br.ule.posto.model.Preco;
import br.ule.posto.model.Produto;
import br.ule.posto.repository.PrecoRepository;
import br.ule.posto.repository.ProdutoRepository;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor @AllArgsConstructor
public class AtualizacaoPrecoForm {
	
	private float valor;
	private Long produtoId;
	
	public Preco atualizar(Long id, PrecoRepository precoRepository, ProdutoRepository produtoRepository) {
		Preco preco = precoRepository.getOne(id);
		Produto produto = produtoRepository.getOne(produtoId);
		preco.setValor(this.valor);
		preco.setProduto(produto);
		
		return preco;
	}
}