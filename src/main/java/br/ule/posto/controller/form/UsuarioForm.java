package br.ule.posto.controller.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.ule.posto.model.Perfil;
import br.ule.posto.model.Usuario;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class UsuarioForm {

	@NotNull @NotEmpty
	private String cpf;
	@NotNull @NotEmpty
	private String nome;
	private Perfil perfil;
	
	public Usuario converter() {
		return new Usuario(cpf, nome, perfil);
	}
}